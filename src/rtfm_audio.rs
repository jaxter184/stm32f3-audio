#![no_std]
#![no_main]

use rtfm::app;
extern crate panic_semihosting;

use stm32f3xx_hal::{
	prelude::*,
	stm32::{self, RCC},
//	rcc::CFGR,
	pac,
	timer::Timer,
};

#[app(device = stm32f3xx_hal::pac)]
const APP: () = {
	static mut DAC_D: gpioa::PA1<Output<PushPull>> = ();
	static mut DAC_T: gpioa::PA1<Output<PushPull>> = ();
	static mut TIMER: Timer<pac::TIM2> = ();

	#[init]
	init() -> init::LateResources {

		let peripherals = stm32::Peripherals::take().unwrap();

		let mut rcc = peripherals.RCC.constrain();
		let mut gpio_a = peripherals.GPIOA.split(&mut rcc.ahb);

		let _dac1_out = (
			gpio_a.pa4.into_analog(&mut gpio_a.moder, &mut gpio_a.pupdr),
			gpio_a.pa5.into_analog(&mut gpio_a.moder, &mut gpio_a.pupdr)
		);

		// enabling dac clk
		let apb1enr = unsafe { &(*RCC::ptr()).apb1enr };
		apb1enr.modify(|_, w| w.dac1en().enabled());

		// set up dac
		let dac = stm32::DAC::ptr();
		let dac_cr = unsafe { &(*dac).cr };
		let dac_swtrigr = unsafe { &(*dac).swtrigr };
		let dac_dhr12rd = unsafe { &(*dac).dhr12rd };

		// enable both dac channels and triggers
		dac_cr.modify(|_, w| w
			.ten1().enabled()
			.ten2().enabled()
			.tsel1().software()
			.tsel2().software()
			.en1().enabled()
			.en2().enabled()
		);

		// Configure the timer.
		let mut timer = device.TIM2.timer(1.hz(), &mut rcc);
		timer.listen();

		// Return the initialised resources.
		init::LateResources {
			DAC_D: dac_dhr12rd,
			DAC_T: dac_swtrigr,
			TIMER: timer,
		}
	}

	#[idle]
	fn idle(_cx: idle::Context) -> ! {
		loop {
			dac_swtrigr.write(|w| w
				.swtrig1().enabled()
				.swtrig2().enabled()
			);
		}
	}

	#[interrupt(resources = [DAC_D, DAC_T, TIMER])]
	fn TIM2() {
		static mut STATE: bool = false;
		static mut I: u16 = 0;

		// Clear the interrupt flag.
		resources.TIMER.clear_irq();
		resources.DAC_D.write(|w| unsafe { w
			.dacc1dhr().bits(i)
			.dacc2dhr().bits(i)
		});
	}
};

