#![no_std]
#![no_main]
//#![deny(unsafe_code)]

extern crate panic_semihosting;

use stm32f3xx_hal::{
	prelude::*,
	stm32::{self, RCC},
};

use cortex_m;
use cortex_m_rt::{entry, exception};

mod audio;

#[entry]
fn main() -> ! {
	let peripherals = stm32::Peripherals::take().unwrap();

	let p = cortex_m::Peripherals::take().unwrap();
	let mut syst = p.SYST;

	// configures the system timer to trigger a SysTick exception every second
	syst.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
	syst.set_reload(audio::SAMPLE_PERIOD); // period = 1s
	syst.enable_counter();
	syst.enable_interrupt();

	let mut rcc = peripherals.RCC.constrain();
	let mut gpio_a = peripherals.GPIOA.split(&mut rcc.ahb);

	let _dac1_out = (
		gpio_a.pa4.into_analog(&mut gpio_a.moder, &mut gpio_a.pupdr),
		gpio_a.pa5.into_analog(&mut gpio_a.moder, &mut gpio_a.pupdr)
	);

	// set up dac
	let apb1enr = unsafe { &(*RCC::ptr()).apb1enr };
	apb1enr.modify(|_, w| w.dac1en().enabled());
	let dac = unsafe { &(*stm32::DAC::ptr()) };
	dac.cr.modify(|_, w| w
		.ten1().enabled()
		.ten2().enabled()
		.tsel1().software()
		.tsel2().software()
		.en1().enabled()
		.en2().enabled()
	);
	
	loop {
		audio::process();
	}
}

#[exception]
fn SysTick() {
	let output = audio::get_sample();
	
	let dac = unsafe { &(*stm32::DAC::ptr()) };
	unsafe {
		dac.dhr12rd.write(|w| w
			.dacc1dhr().bits(output)
			.dacc2dhr().bits(output)
		);
	}
	dac.swtrigr.write(|w| w
		.swtrig1().enabled()
		.swtrig2().enabled()
	);
}
