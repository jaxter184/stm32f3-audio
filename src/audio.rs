pub const SAMPLE_PERIOD: u32 = 100;

const BUFFER_SIZE: usize = 256;
const NUM_BUFFERS: usize = 3;
static mut INDEX: usize = 0;
static mut BUFFER_IN_USE: usize = 0;
static mut BUFFER_READY: [bool; NUM_BUFFERS] = [false; NUM_BUFFERS];
static mut BUFFER: [[u16; BUFFER_SIZE]; NUM_BUFFERS] = [[0; BUFFER_SIZE]; NUM_BUFFERS];

static mut CURRENT_SAMPLE: usize = 0;

pub fn get_sample() -> u16 {
	let mut output: u16 = 0;
	unsafe {
		if BUFFER_READY[BUFFER_IN_USE] {
			output = BUFFER[BUFFER_IN_USE][INDEX];
			INDEX += 1;
			if INDEX >= BUFFER_SIZE {
				INDEX = 0;
				BUFFER_READY[BUFFER_IN_USE] = false;
				BUFFER_IN_USE = (BUFFER_IN_USE + 1) % NUM_BUFFERS;
			}
		}
	}
	output
}

pub fn process() {
	unsafe {
		let mut processing_idx = BUFFER_IN_USE;
		loop {
			if !BUFFER_READY[processing_idx] {
				saw(&mut BUFFER[processing_idx]);
				BUFFER_READY[processing_idx] = true;
			}
			processing_idx = (processing_idx + 1) % NUM_BUFFERS;
			if processing_idx == BUFFER_IN_USE {
				break;
			}
		}
	}
}

pub fn saw(buffer: &mut[u16]) {
	unsafe {
		for ea_idx in 0..BUFFER_SIZE {
			buffer[ea_idx] = (CURRENT_SAMPLE*8 & 0xfff) as u16;
			CURRENT_SAMPLE += 1;
		}
	}
}
